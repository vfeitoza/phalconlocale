<?php
namespace ModDev\PhalconLocale;

use Phalcon\DI\Injectable as DiInjectable;
use Phalcon\Http\Response;

/**
 * Class to best determine a client's locale based on the avalable locales set up in the application config.
 *
 * @author mgarrett
 */
class PhalconLocale extends DiInjectable
{

    /**
     * Used to name routes that are already locale-aware. Hopefully, this will be the name for the routing group some
     * time in the future.
     * @var string
     */
    const LOCALE_AWARE_ROUTE = 'LocaleAwareRoute';

    /**
     * These are browser languages that many have as default, even if the user is not in the US.
     * @var array
     */
    private $browserDefaultLangs = array('en', 'en_US');

    /**
     * Hold IP internally - useful if we want to do GeoIP testing with IP from another country
     * @var string
     */
    private $clientIp;

    /**
     * Store the config data without setting a DI variable that could cause issues
     *
     * @var \Phalcon\Config
     */
    private $localeConfig = null;

    /**
     * These are country => language mappings cribbed from CLDR 24. Largest language population gets the country!
     * @see http://unicode.org/repos/cldr/trunk/common/supplemental/supplementalData.xml
     * @var array
     */
    private $territoryLanguageMapping = array(
        'AC' => 'en', 'AD' => 'ca', 'AE' => 'ar', 'AF' => 'fa', 'AG' => 'en', 'AI' => 'en', 'AL' => 'sq', 'AM' => 'hy',
        'AO' => 'pt', 'AQ' => 'und', 'AR' => 'es', 'AS' => 'sm', 'AT' => 'de', 'AU' => 'en', 'AW' => 'nl', 'AX' => 'sv',
        'AZ' => 'az', 'BA' => 'bs', 'BB' => 'en', 'BD' => 'bn', 'BE' => 'nl', 'BF' => 'mos', 'BG' => 'bg', 'BH' => 'ar',
        'BI' => 'rn', 'BJ' => 'fr', 'BL' => 'fr', 'BM' => 'en', 'BN' => 'ms', 'BO' => 'es', 'BQ' => 'pap', 'BR' => 'pt',
        'BS' => 'en', 'BT' => 'dz', 'BV' => 'und', 'BW' => 'en', 'BY' => 'be', 'BZ' => 'en', 'CA' => 'en', 'CC' => 'ms',
        'CD' => 'sw', 'CF' => 'fr', 'CG' => 'fr', 'CH' => 'de', 'CI' => 'fr', 'CK' => 'en', 'CL' => 'es', 'CM' => 'fr',
        'CN' => 'zh', 'CO' => 'es', 'CP' => 'und', 'CR' => 'es', 'CU' => 'es', 'CV' => 'kea', 'CW' => 'pap',
        'CX' => 'en', 'CY' => 'el', 'CZ' => 'cs', 'DE' => 'de', 'DG' => 'en', 'DJ' => 'aa', 'DK' => 'da', 'DM' => 'en',
        'DO' => 'es', 'DZ' => 'ar', 'EA' => 'es', 'EC' => 'es', 'EE' => 'et', 'EG' => 'ar', 'EH' => 'ar', 'ER' => 'ti',
        'ES' => 'es','ET' => 'en', 'FI' => 'fi', 'FJ' => 'en', 'FK' => 'en', 'FM' => 'chk', 'FO' => 'fo', 'FR' => 'fr',
        'GA' => 'fr', 'GB' => 'en', 'GD' => 'en', 'GE' => 'ka', 'GF' => 'fr', 'GG' => 'en', 'GH' => 'ak', 'GI' => 'en',
        'GL' => 'kl', 'GM' => 'en', 'GN' => 'fr', 'GP' => 'fr', 'GQ' => 'es', 'GR' => 'el', 'GS' => 'und', 'GT' => 'es',
        'GU' => 'en', 'GW' => 'pt', 'GY' => 'en', 'HK' => 'zh', 'HM' => 'und', 'HN' => 'es', 'HR' => 'hr', 'HT' => 'ht',
        'HU' => 'hu', 'IC' => 'es', 'ID' => 'id', 'IE' => 'en', 'IL' => 'he', 'IM' => 'en', 'IN' => 'hi', 'IO' => 'en',
        'IQ' => 'ar', 'IR' => 'fa', 'IS' => 'is', 'IT' => 'it', 'JE' => 'en', 'JM' => 'en', 'JO' => 'ar', 'JP' => 'ja',
        'KE' => 'en', 'KG' => 'ky', 'KH' => 'km', 'KI' => 'en', 'KM' => 'ar', 'KN' => 'en', 'KP' => 'ko', 'KR' => 'ko',
        'KW' => 'ar', 'KY' => 'en', 'KZ' => 'ru', 'LA' => 'lo', 'LB' => 'ar', 'LC' => 'en', 'LI' => 'de', 'LK' => 'si',
        'LR' => 'en', 'LS' => 'st', 'LT' => 'lt', 'LU' => 'fr', 'LV' => 'lv', 'LY' => 'ar', 'MA' => 'ar', 'MC' => 'fr',
        'MD' => 'ro', 'ME' => 'sr', 'MF' => 'fr', 'MG' => 'mg', 'MH' => 'en', 'MK' => 'mk', 'ML' => 'bm', 'MM' => 'my',
        'MN' => 'mn', 'MO' => 'zh', 'MP' => 'en', 'MQ' => 'fr', 'MR' => 'ar', 'MS' => 'en', 'MT' => 'mt', 'MU' => 'mfe',
        'MV' => 'dv', 'MW' => 'en', 'MX' => 'es', 'MY' => 'ms', 'MZ' => 'pt', 'NA' => 'af', 'NC' => 'fr', 'NE' => 'ha',
        'NF' => 'en', 'NG' => 'en', 'NI' => 'es', 'NL' => 'nl', 'NO' => 'nb', 'NP' => 'ne', 'NR' => 'en', 'NU' => 'en',
        'NZ' => 'en', 'OM' => 'ar','PA' => 'es', 'PE' => 'es', 'PF' => 'fr', 'PG' => 'tpi', 'PH' => 'en', 'PK' => 'ur',
        'PL' => 'pl', 'PM' => 'fr', 'PN' => 'en', 'PR' => 'es', 'PS' => 'ar', 'PT' => 'pt', 'PW' => 'pau', 'PY' => 'gn',
        'QA' => 'ar', 'RE' => 'fr', 'RO' => 'ro', 'RS' => 'sr', 'RU' => 'ru', 'RW' => 'rw', 'SA' => 'ar', 'SB' => 'en',
        'SC' => 'crs', 'SD' => 'ar', 'SE' => 'sv', 'SG' => 'en', 'SH' => 'en', 'SI' => 'sl', 'SJ' => 'nb', 'SK' => 'sk',
        'SL' => 'kri', 'SM' => 'it', 'SN' => 'fr', 'SO' => 'so', 'SR' => 'nl', 'SS' => 'ar', 'ST' => 'pt', 'SV' => 'es',
        'SX' => 'en', 'SY' => 'ar', 'SZ' => 'en', 'TA' => 'en', 'TC' => 'en', 'TD' => 'fr', 'TF' => 'fr', 'TG' => 'fr',
        'TH' => 'th', 'TJ' => 'tg', 'TK' => 'en', 'TL' => 'pt', 'TM' => 'tk', 'TN' => 'ar', 'TO' => 'to', 'TR' => 'tr',
        'TT' => 'en', 'TV' => 'tvl', 'TW' => 'zh', 'TZ' => 'sw', 'UA' => 'uk', 'UG' => 'sw', 'UM' => 'en', 'US' => 'en',
        'UY' => 'es', 'UZ' => 'uz', 'VA' => 'it', 'VC' => 'en', 'VE' => 'es', 'VG' => 'en', 'VI' => 'en', 'VN' => 'vi',
        'VU' => 'bi', 'WF' => 'wls', 'WS' => 'sm', 'XK' => 'sq', 'YE' => 'ar', 'YT' => 'swb', 'ZA' => 'en',
        'ZM' => 'en', 'ZW' => 'en'
    );

    /**
     * Constructor. Will return false if the minimum requirements aren't met.
     *
     * @param \Phalcon\DiInterface $dependencyInjector (Optional) The DI to use.
     * @param \Phalcon\Config $config (Optional) The Config object to use. Will need a 'locale' key with appropriate
     *                                options set.
     * @throws \InvalidArgumentException Throws if there is no valid config either passed, or is a service in the DI
     * @return boolean|self
     */
    public function __construct(\Phalcon\DiInterface $dependencyInjector = null, \Phalcon\Config $config = null)
    {
        if ($dependencyInjector && is_a($dependencyInjector, '\Phalcon\DI')) {
            $this->setDI($dependencyInjector);
        }

        if (empty($config) && $this->di->has('config')) {
            $this->localeConfig = $this->di->get('config');
        } elseif (!is_object($config) || !is_a($config, '\Phalcon\Config')) {
            throw new \InvalidArgumentException(
                'Config needs to be defined in your DI, or passed as a Phalcon\Config object',
                500
            );
        } else {
            $this->localeConfig = $config;
        }

        if (!$this->installMeetsMinimumRequirements($this->di, $this->localeConfig)) {
            return false;
        }
    }

    /**
     * Function to reroute request to a locale-aware route by prepending the best locale for the client to the incoming
     * request uri.
     *
     * @param \Phalcon\DiInterface $dependancyInjector
     * @param \Phalcon\Config $config
     * @return mixed
     */
    public static function urlRedirect(\Phalcon\DiInterface $dependancyInjector, \Phalcon\Config $config)
    {
        $locale = null;

        // We don't want locale-based redirects
        if (!$config->locale->localeBasedRedirect) {
            return false;
        }

        //If route is already has locale, we don't need to do anything.
        $router = $dependancyInjector->getShared('router');
        $router->handle();

        if ($router->getMatchedRoute() && $router->getMatchedRoute()->getName() == self::LOCALE_AWARE_ROUTE) {
            return false;
        }

        if (!self::installMeetsMinimumRequirements($dependancyInjector, $config)) {
            $dependancyInjector->getShared('flash')->error(
                'Cannot do locale-based redirection. Please verify your install meets minimum requirements.'
            );
        }

        //Check is we've already stored the locale in session. If not, instantiate class to find the best one.
        $session = $dependancyInjector->getSession();
        if ($session->has($config->locale->sessionKey)) {
            $locale = $session->get($config->locale->sessionKey);
        } else {
            $localeObject = new self($dependancyInjector, $config);
            $locale = $localeObject->getLocale();
        }

        $response = new Response();
        $response->redirect($locale . $router->getRewriteUri(), false);
        $response->send();
    }

    /**
     * Retrieves the locale for this user
     *
     * @return string
     */
    public function getLocale()
    {
        $locale = null;

        if ($this->session->has($this->localeConfig->locale->sessionKey)) {
            return $this->session->get($this->localeConfig->locale->sessionKey);
        }

        $locale = \Locale::canonicalize($this->determineBestAvailableLocale());
        $this->session->set($this->localeConfig->locale->sessionKey, $locale);

        return $locale;
    }

    /**
     * Determines the best locale for the request. Does optional GeoIP checking and double checks browser default langs
     * based on configuration variables. To retrieve the locale externally, use the {@see getLocale()} function.
     *
     * @return string
     */
    private function determineBestAvailableLocale()
    {
        $locale = null;
        $browserLang = \Locale::canonicalize($this->request->getBestLanguage());

        /**
         * See if the first language in the Accept-Language header is something we want. Optionally skip if we want to
         * double check "default" languages that most browsers are set to on download.
         */
        if (!in_array($browserLang, $this->browserDefaultLangs)
            || !$this->localeConfig->locale->doubleCheckBrowserDefaultLangs) {

            $locale = \Locale::lookup(
                (array) $this->localeConfig->locale->availableLocales,
                $browserLang,
                true,
                false
            );

            if ($locale) {
                return $locale;
            }
        }

        /**
         * The browser locale isn't in our available locales. Do a GeoIP check, but if that doesn't work or is not
         * enabled, return default locale.
         */
        if ($geoLocale = $this->getGeoLocale()) {
            $locale = \Locale::lookup(
                (array) $this->localeConfig->locale->availableLocales,
                $geoLocale,
                true,
                false
            );
            if ($locale) {
                return $locale;
            }
        }

        /**
         * GeoIP locale didn't help, so we loop through ALL Accept-Language Headers. If none of those work, default.
         */
        preg_match_all(
            '/([a-z]{1,8}(-[[:alnum:]]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i',
            $this->request->getHeader('ACCEPT_LANGUAGE'),
            $parsedLangHeader
        );

        foreach ($parsedLangHeader[1] as $lang) {
            $locale = \Locale::lookup(
                (array) $this->localeConfig->locale->availableLocales,
                $lang,
                true,
                false
            );

            if ($locale) {
                return $locale;
            }
        }

        return $this->localeConfig->locale->defaultLocale;
    }

    /**
     * Function used to get the GeoIP-designated locale for the client IP. Will return false if IP can't be found in
     * the GeoIP database. Uses {@see $territoryLanguageMapping} to relate country to language.
     *
     * @return boolean|string
     */
    public function getGeoLocale()
    {
        if (!$this->localeConfig->locale->useGeoIP) {
            return false;
        }

        $geoCountry = @geoip_country_code_by_name($this->getClientIp());

        if (!$geoCountry) {
            return false;
        }

        return \Locale::canonicalize($this->territoryLanguageMapping[$geoCountry] . '_' . $geoCountry);

    }

    /**
     * Set the client IP. Useful to do manually for testing. Not passing an IP will use
     * $this->request->getClientAddress(true);
     *
     * @param string $ip (optional)
     * @return null
     */
    public function setClientIp ($ip = null)
    {
        $this->clientIp = (filter_var($ip, FILTER_VALIDATE_IP)) ? $ip : $this->request->getClientAddress(true);
    }

    /**
     * Return the client IP.
     *
     * @return string
     */
    public function getClientIp()
    {
        if (!$this->clientIp) {
            $this->setClientIp();
        }
        return $this->clientIp;
    }

    /**
     * Make sure that we have the needed extensions to to the locale class to work
     *
     * @param \Phalcon\DiInterface $dependancyInjector The DI to use. Needs 'flash' service set.
     * @param \Phalcon\Config $config Application config
     * @return boolean
     */
    private static function installMeetsMinimumRequirements(\Phalcon\DiInterface $dependancyInjector, \Phalcon\Config $config)
    {
        if (!extension_loaded('intl')) {
            $dependancyInjector->getShared('flash')->error(
                'Cannot set a locale. Please verify you have the "intl" extension loaded'
            );
            return false;
        }

        if ($config->locale->useGeoIP
            && (!extension_loaded('geoip') || !geoip_db_avail($config->locale->geoIPEdition))) {

            $dependancyInjector->getShared('flash')->error(
                'Cannot use GeoIP. Please verify you have the "geoip" extension loaded and the correct GeoIP database '
                .'installed, or turn off this option.'
            );
            return false;
        }

        return true;
    }
}
